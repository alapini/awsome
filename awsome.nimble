# Package

version       = "0.1.0"
author        = "ryukoposting"
description   = "Unofficial Amazon Web Services SDK"
license       = "Apache-2.0"
srcDir        = "src"


# Dependencies

requires "nim >= 0.19.4"

task test, "run all tests":
  withDir "tests":
    exec "nim c -d:ssl -r test1.nim"
