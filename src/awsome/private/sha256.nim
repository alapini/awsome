import strutils
 
const
  sha256Len = 32
 
proc sha256(d: cstring, n: culong, md: cstring = nil): cstring {.cdecl, dynlib: "libssl.so", importc.}
 
proc sha256(s: string): string =
  result = ""
  let s = sha256(s.cstring, s.len.culong)
  for i in 0 ..< sha256Len:
    result.add s[i].BiggestInt.toHex(2).toLower
 
# echo SHA256("Rosetta code")
