import sugar, httpclient, uri, tables, sha256, strutils

type
  ReqType* = enum
    # is this already in the stdlib?
    rtGet = "GET",
    rtPost = "POST",
    rtPut = "PUT",
    rtDelete = "DELETE",
    rtOptions = "OPTIONS",
    rtTrace = "TRACE",
    rtPatch = "PATCH"


proc genQueryStr*(pairs: varargs[(string,string)]): string =
  ## just takes a bunch of pairs of strings and puts them together
  ## into a properly formatted query string, so for an input like
  ##
  ## .. code-block:: nim
  ## genQueryStr(("foo", "bar"), ("hello", "w orld"))
  ##
  ## the output would be "foo=bar&hello=w%20rld"
  result = ""
  for p in pairs:
    result.add(p[0] & "=" & encodeUrl(p[1], usePlus = off) & "&")
  result = result[0..result.high - 1]
  
#[
#TODO: sort pairs by code point for request hash
proc request*(
    service: Service,
    rt: ReqType,
    path: string,
    pairs: varargs[(string, string)]): string =
  let
    host =
      service & ".amazonaws.com"
    qStr =
      genQueryStr(pairs)
    canonicalRequest =
      rt & '\n' &
      path & '\n' &     # path <- "Canonical URI," needs additional processing?
      canonicalQueryString + '\n' &
      canonicalHeaders & '\n' &
      signedHeaders & '\n' &
      requestPayload.sha256.toHex
]#
