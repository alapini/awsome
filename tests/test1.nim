# This is just an example to get you started. You may wish to put all of your
# tests into a single file, or separate them into multiple `test1`, `test2`
# etc. files (better names are recommended, just make sure the name starts with
# the letter 't').

import unittest, httpclient

import awsome, awsome/private/requests

test "can add":
  check add(5, 5) == 10


let
  client = newHttpClient()
  reqstr =
    "http://ec2.amazonaws.com/?" & genQueryStr(
      ("Action", "AllocateHosts"),
      ("Version", "2016-11-15"), # v4?
      ("AvailabilityZone", "us-east-1a"),
      ("InstanceType", "t1.micro"),
      ("Quantity", "1"))

echo "making request to: ", reqstr

let resp = client.post(reqstr)

echo "\nversion=", resp.version
echo "\nstatus=", resp.status
echo "\nheaders=", resp.headers
echo "\nbody=", resp.body
